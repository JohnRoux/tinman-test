<div class= "product" data-id="{{$product['id']}}">
    @if(isset($product['category']))
        <p class="product-category product-title">{{$product['category']['data']['attributes']['title']}} - {{$product['attributes']['title']}}</p>
    @else
        <p class="product-title">{{$product['attributes']['title']}}</p>
    @endif
    <p class="product-description">{{$product['attributes']['description']}}</p>
    <img src="{{$product['attributes']['image_url']}}">
    <p class="product-price">R{{$product['attributes']['price']}}</p>
</div>