<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .title {
            font-size: 84px;
            display: flex;
            justify-content: center;
        }

        .product{

            margin-top: 150px;
            margin-right: 20px;
            float: left;
            width: 400px;
            height: 500px;
            border: 1px grey solid;
            display:inline-block;
            position: relative;
        }
        .product .product-title{
            display:inline-block;
            color:black;
            font-weight: 800;
            text-decoration: underline;

        }
        .product .product-category{

            display:inline-block;
            text-align:right;
            color:black;
            margin: 0;
            padding: 10px;
        }

        .product img{
            max-width:90%;
            max-height:100%;
        }

        .product .product-price{
            position:absolute;
            bottom: 0;
            right:0;
            color:red;
            font-style: italic;
            font-size: 2em;
            margin-bottom: 0;
            padding: 5px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title"> Catalogue</div>
    @foreach($products as $product)
            @component('components.product', ["product"=>$product])
            @endComponent
        @endforeach
    </div>
</div>
</body>
</html>
