<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth', 'namespace' => 'App\Http\Controllers\Auth'], function(Router $api) {
        $api->post('signup', 'SignUpController@signUp');
        $api->post('login', 'LoginController@login');

        $api->post('recovery', 'ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'ResetPasswordController@resetPassword');

        $api->post('logout', 'LogoutController@logout');
        $api->post('refresh', 'RefreshController@refresh');
        $api->get('me', 'UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth', 'namespace' => 'App\Http\Controllers\V1'], function(Router $api) {
	    $api->post('products', 'ProductsController@store');
	    $api->post('categories', 'CategoriesController@store');

    });

	$api->group(['namespace' => 'App\Http\Controllers\V1'], function(Router $api) {
		$api->get('products', 'ProductsController@index');
		$api->get('categories', 'CategoriesController@index');

	});
});
