<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Product;

/**
 * Class ProductTransformer.
 *
 * @package namespace AppTransformers;
 */
class ProductTransformer extends TransformerAbstract
{

	/**
	 * @var array
	 */
	protected $availableIncludes = [
		'category',
		];
    /**
     * Transform the Product entity.
     *
     * @param \App\Entities\Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
	    $data = array_only($model->toArray(), $model->getFillable());
	    return [
		    'id'         => (int) $model->id,
		    'type' => $model->getModelName(),
		    'attributes' => $data
	    ];
    }

	/**
	 * @param \App\Entities\Product $model
	 *
	 * @return \League\Fractal\Resource\Item|null
	 */
	public function includeCategory(Product $model)
	{
		if(is_null($model->category_id)) {
			return null;
		}
		return $this->item($model->category, new CategoryTransformer());
	}

}
