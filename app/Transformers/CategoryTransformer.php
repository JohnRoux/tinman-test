<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Category;

/**
 * Class CategoriesTransformer.
 *
 * @package namespace AppTransformers;
 */
class CategoryTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
		'products',
	];

    /**
     * Transform the Categories entity.
     *
     * @param \App\Entities\Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
	    $data = array_only($model->toArray(), $model->getFillable());
	    return [
		    'id'         => (int) $model->id,
		    'type' => $model->getModelName(),
		    'attributes' => $data
	    ];

    }

	public function includeProducts(Category $model)
	{
		if(is_null($model->products)) {
			return null;
		}
		return $this->collection($model->products, new ProductTransformer());
	}
}
