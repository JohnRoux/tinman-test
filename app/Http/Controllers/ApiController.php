<?php


namespace App\Http\Controllers;

use Illuminate\Http\Response;

/**
 * Class ApiController
 * @package App\Http\Api
 */
class ApiController extends Controller
{

	/**
	 * @var int
	 */
	protected $statusCode = Response::HTTP_OK;

	/**
	 * @return int
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * @param int $statusCode
	 * @return $this
	 */
	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;
		return $this;
	}

	/**
	 * @param $data
	 * @param array $headers
	 * @return mixed
	 */
	public function respond($data, $headers = [])
	{
		return response()->json($data, $this->getStatusCode(), $headers);
	}

	/**
	 * @param $resource
	 * @return mixed
	 */
	public function respondWithSuccess($resource = [])
	{
		return $this->respond($resource);
	}

	/**
	 * @param $message
	 * @return mixed
	 */
	public function respondWithError($message)
	{
		return $this->respond([
			'errors' => [
				'status' => $this->getStatusCode(),
				'errors' => $message
			]
		]);
	}

	/**
	 * @return mixed
	 * @param $message
	 */
	public function respondForbidden($message = 'Access forbidden')
	{
		$this->setStatusCode(Response::HTTP_FORBIDDEN);
		return $this->respondWithError($message);
	}

	/**
	 * @return mixed
	 * @param $message
	 */
	public function respondUnauthorized($message = 'The requested resource failed authorization')
	{
		$this->setStatusCode(Response::HTTP_UNAUTHORIZED);
		return $this->respondWithError($message);
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondNotFound($message = 'The requested resource could not be found')
	{
		return $this->setStatusCode(Response::HTTP_NOT_FOUND)->respondWithError($message);
	}


	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondInternalError($message = 'An internal server error has occurred')
	{
		$this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
		return $this->respondWithError($message);
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondConfigInternalError($message = [])
	{
		$this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
		return $this->respondWithError($message);
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondUnprocessableEntity($message = 'The request cannot be processed with the given parameters')
	{
		$this->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
		return $this->respondWithError($message);
	}


	/**
	 * @param array $resource
	 * @return mixed
	 */
	public function respondCreated($resource = [])
	{
		return $this->setStatusCode(Response::HTTP_CREATED)->respondWithSuccess($resource);
	}


	/**
	 * @param array $resource
	 * @return mixed
	 */
	public function respondUpdated($resource = [])
	{
		return $this->setStatusCode(Response::HTTP_OK)->respondWithSuccess($resource);
	}

	/**
	 * @return mixed
	 */
	public function respondNoContent()
	{
		return $this->setStatusCode(Response::HTTP_NO_CONTENT)->respondWithSuccess();
	}

	/**
	 * @param null $message
	 * @return mixed
	 */
	public function respondHttpConflict($message = null)
	{
		return $this->setStatusCode(Response::HTTP_CONFLICT)->respondWithError($message);
	}


	// Base Documentation
	/**
	 * @SWG\Swagger(
	 *     basePath=L5_SWAGGER_CONST_BASE,
	 *     schemes={"https"},
	 *     host=L5_SWAGGER_CONST_HOST,
	 *     @SWG\Info(
	 *         version=L5_SWAGGER_VERSION,
	 *         title=L5_SWAGGER_CONST_TITLE,
	 *     ),
	 *     consumes={"application/x-www-form-urlencoded", "application/json"},
	 *     produces={"application/json"},
	 * )
	 */

	// Security definition using a JWT Token
	/**
	 * @SWG\SecurityScheme(
	 *   securityDefinition="bearer",
	 *   type="apiKey",
	 *   in="header",
	 *   name="Authorization"
	 * )
	 */


	// Base Collection and Item defined
	/**
	 * @SWG\Definition(
	 *     definition="Item",
	 *     required={"id", "type","attributes"},
	 *     @SWG\Property(
	 *          property="id",
	 *          type="integer",
	 *     ),
	 *     @SWG\Property(
	 *          property="type",
	 *          type="string"
	 *     ),
	 *     @SWG\Property(
	 *          property="attributes",
	 *          type="array",
	 *          @SWG\Items()
	 *     ),
	 * )
	 *     )
	 * )
	 */

	/**
	 * @SWG\Definition(
	 *     definition="ItemCollection",
	 *     required={"data"},
	 *     @SWG\Property(
	 *          property="data",
	 *          type="array",
	 *          @SWG\Items(ref="#/definitions/Item")
	 *     ),
	 * )
	 *     )
	 * )
	 */

	/**
	 * @SWG\Response(
	 *     response="ModelNotFound",
	 *     description="Unable to find model",
	 * )
	 *
	 * @SWG\Response(
	 *     response="ErrorUnprocessableEntity",
	 *     description="Error with input, see error databags"
	 * )
	 */


}