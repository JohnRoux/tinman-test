<?php

namespace App\Http\Controllers;

use Dingo\Api\Facade\Route;
use Dingo\Api\Http\Request;


/**
 * Class HomeController.
 *
 * @package namespace AppHttp\Controllers;
 */
class HomeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function catalogue(Request $request)
	{
		$request = Request::create('api/products', 'GET', $request->all());
		$response = Route::dispatch($request);
		$products = json_decode($response->getContent(), true)['data'];
		return view('tinman.catalogue', ["products"=>$products]);
	}

}
