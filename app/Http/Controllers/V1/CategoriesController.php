<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\ApiController;
use App\Presenters\CategoryPresenter;
use App\Repositories\CategoryRepository;

/**
 * Class CategoriesController.
 *
 * @package namespace AppHttp\Controllers;
 */
class CategoriesController extends ApiController
{
	/**
	 * @var CategoryRepository
	 */
	protected $repository;

	/**
	 * @var \App\Presenters\CategoryPresenter
	 */
	protected $presenter;


	/**
	 * CategoriesController constructor.
	 *
	 * @param CategoryRepository $repository
	 */
	public function __construct(CategoryRepository $repository, CategoryPresenter $presenter)
	{
		$this->repository = $repository;
		$this->presenter = $presenter;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$result = $this->repository->all();
		return $this->presenter->present($result);
	}
}
