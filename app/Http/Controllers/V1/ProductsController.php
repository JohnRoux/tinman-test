<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\ApiController;
use App\Presenters\ProductPresenter;
use Illuminate\Http\Request;
use App\Http\Requests\ProductCreateRequest;
use App\Repositories\ProductRepository;

/**
 * Class ProductsController.
 *
 * @package namespace AppHttp\Controllers;
 */
class ProductsController extends ApiController
{
	/**
	 * @var ProductRepository
	 */
	protected $repository;
	/**
	 * @var \App\Presenters\ProductPresenter
	 */
	protected $presenter;


	/**
	 * ProductsController constructor.
	 *
	 * @param ProductRepository $repository
	 */
	public function __construct(ProductRepository $repository, ProductPresenter $presenter)
	{
		$this->repository = $repository;
		$this->presenter = $presenter;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$products = $this->repository->all();
		return $this->respondWithSuccess($this->presenter->present($products));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  ProductCreateRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @throws \Prettus\Validator\Exceptions\ValidatorException
	 */
	public function store(ProductCreateRequest $request)
	{
		$product = $this->repository->create($request->all());
		return $this->respondCreated($this->presenter->present($product));
	}

}
