<?php

namespace App\Entities;

use App\Entities\Category;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Product.
 *
 * @package namespace AppEntities;
 */
class Product extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'title',
	    'description',
	    'price',
	    'image_url',
	    'category_id'
    ];

	protected $modelName = 'Product';

    public function category(){
    	return $this->belongsTo(Category::class);
    }

	public function getModelName()
	{
		return $this->modelName;
	}
}
