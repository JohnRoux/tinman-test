<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoriesRepository.
 *
 * @package namespace AppRepositories;
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
