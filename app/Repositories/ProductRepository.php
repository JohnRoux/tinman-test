<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository.
 *
 * @package namespace AppRepositories;
 */
interface ProductRepository extends RepositoryInterface
{
    //
}
