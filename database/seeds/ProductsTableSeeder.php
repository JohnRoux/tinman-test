<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    \App\Entities\Product::create([
		    'title' => 'Trail running blaze',
		    'description' => 'Keep left pass right',
		    'category_id' => 1,
		    'image_url' => "https://www.rei.com/media/a71fce1b-42f7-4e71-905e-464a3bb00edf?size=784x588",
		    'price' => "699.99"
	    ]);

	    \App\Entities\Product::create([
		    'title' => 'High Heels',
		    'description' => 'Made to compress your lovely feet',
		    'category_id' => 1,
		    'image_url' => "https://www.dhresource.com/webp/m/0x0s/f2-albu-g3-M01-CB-9B-rBVaHVYCP1uAFQyKAAKdblKXeho605.jpg/platform-high-heel-shoes-for-ladies-summer-style-black-stiletto-heel-shoes-round-toes-designer-dress-shoes-for-blp1001-8.jpg",
		    'price' => "1999.99"
	    ]);

	    \App\Entities\Product::create([
		    'title' => 'Sphero',
		    'description' => 'Fun IoT device thingdig',
		    'category_id' => 2,
		    'image_url' => "https://store.storeimages.cdn-apple.com/4667/as-images.apple.com/is/image/AppleInc/aos/published/images/H/K8/HK8S2/HK8S2?wid=572&hei=572&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1509146650697",
		    'price' => "1699.99"
	    ]);
    }
}
