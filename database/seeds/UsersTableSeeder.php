<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entities\User::create([
	        'name' => 'admin',
	        'email' => 'john.roux1@gmail.com',
	        'password' => 'secret',
        ]);

	    \App\Entities\User::create([
		    'name' => 'user',
		    'email' => 'user@tinman.co.za',
		    'password' => 'secret2',
	    ]);
    }
}
