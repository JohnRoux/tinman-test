<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    \App\Entities\Category::create([
		    'title' => 'Shoes',
		    'description' => 'Some dope shoes',
	    ]);

	    \App\Entities\Category::create([
		    'title' => 'Gadgets',
		    'description' => 'Random stuff you\'ll likely use once and then store away forever!',
	    ]);

	    \App\Entities\Category::create([
		    'title' => 'Time',
		    'description' => 'Just kidding, no products available here :joy:',
	    ]);
    }
}
