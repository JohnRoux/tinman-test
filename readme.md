# Tinman test

# Installation
 * clone
 * composer install
 * run `php artisan key:generate`
 * run `cp .env.example .env`
 * Update `.env` with your own db details
 * run `php artisan migrate && php artisan db:seed`
 * run `php artisan serve` to test it out
 
 
 
# Current features

## Web:
 1. Home Page (Catalogue)
    * searchable via any [respository search parameters](https://github.com/andersao/l5-repository#example-the-criteria)
    
    
## API:
 1. Login
    * Using JWT auth token
    * parse Username and password to {{url}}/api/auth/login and it will return your token
    * In all auth required requests, add Authorization header with `BEARER {{jwt_token}}`
     
 2. Products
    * Index call is open to all
    * Create call requires JWT Auth
    * searchable via any [respository search parameters](https://github.com/andersao/l5-repository#example-the-criteria)
    
 3. Categories
    * Index call is open to all
    * searchable via any [respository search parameters](https://github.com/andersao/l5-repository#example-the-criteria)
    
    
    
# Current limitations

FE is shocking.
For a usable system, user creation / management would be wanted
Also updateing/deleting products/categories

Some caching on the index calls would be good, this repository pattern allows for some cool stuff in that regard!
    